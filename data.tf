# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

data "google_storage_project_service_account" "this" {}

data "google_client_config" "provider" {}

data "google_container_cluster" "dyff" {
  name     = local.cluster_name
  location = local.region
}
