# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_storage_class" "regional_standard_rwo" {
  metadata {
    name = "regional-standard-rwo"
  }

  parameters = {
    "replication-type" = "regional-pd"
    "type"             = "pd-balanced"
  }

  reclaim_policy      = "Retain"
  storage_provisioner = "pd.csi.storage.gke.io"
  volume_binding_mode = "WaitForFirstConsumer"
}

resource "kubernetes_storage_class" "regional_premium_rwo" {
  metadata {
    name = "regional-premium-rwo"
  }

  parameters = {
    "replication-type" = "regional-pd"
    "type"             = "pd-ssd"
  }

  reclaim_policy      = "Retain"
  storage_provisioner = "pd.csi.storage.gke.io"
  volume_binding_mode = "WaitForFirstConsumer"
}


resource "kubernetes_storage_class" "retain_standard_rwo" {
  metadata {
    name = "retain-standard-rwo"
  }

  parameters = {
    "type" = "pd-balanced"
  }

  reclaim_policy      = "Retain"
  storage_provisioner = "pd.csi.storage.gke.io"
  volume_binding_mode = "WaitForFirstConsumer"
}
