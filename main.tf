# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "random_string" "suffixes" {
  for_each = local.bucket_ids

  length = 8

  special = false
  upper   = false
}

resource "google_kms_key_ring" "keyring" {
  name     = local.name
  location = local.region
}

resource "google_kms_crypto_key" "key" {
  name            = local.name
  key_ring        = google_kms_key_ring.keyring.id
  rotation_period = "7776000s"
}

resource "google_kms_crypto_key_iam_binding" "binding" {
  crypto_key_id = google_kms_crypto_key.key.id
  role          = "roles/cloudkms.cryptoKeyEncrypterDecrypter"

  members = [
    "serviceAccount:${data.google_storage_project_service_account.this.email_address}",
  ]
}

resource "google_storage_bucket" "dyff" {
  for_each = local.bucket_ids

  name = "dyff-${each.key}-${random_string.suffixes[each.key].result}"

  location = local.region

  storage_class = "STANDARD"

  uniform_bucket_level_access = true

  public_access_prevention = "enforced"

  labels = local.default_tags

  cors {
    origin          = var.google_cloud_storage_allowed_cors_origins
    method          = ["GET", "HEAD", "PUT", "POST", "DELETE"]
    response_header = ["*"]
  }

  encryption {
    default_kms_key_name = google_kms_crypto_key.key.id
  }

  soft_delete_policy {
    retention_duration_seconds = 604800 # 7 days
  }

  versioning {
    enabled = true
  }

  lifecycle_rule {
    condition {
      days_since_noncurrent_time = 7
    }
    action {
      type = "Delete"
    }
  }

  lifecycle_rule {
    condition {
      age = 1
    }
    action {
      type = "AbortIncompleteMultipartUpload"
    }
  }

  depends_on = [google_kms_crypto_key_iam_binding.binding]
}

resource "google_service_account" "dyff" {
  account_id   = local.name
  display_name = local.name
}

resource "google_project_iam_member" "storage_admin" {
  project = var.project
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.dyff.email}"
}

resource "google_storage_hmac_key" "key" {
  service_account_email = google_service_account.dyff.email
}

resource "google_storage_bucket_iam_member" "ksa_inferencesession_runner_models_viewer" {
  bucket = google_storage_bucket.dyff["models"].name
  role   = "roles/storage.objectViewer"
  # TODO: Don't hard-code KSA name
  member = "serviceAccount:${var.project}.svc.id.goog[workflows/inferencesession-runner]"
}

resource "google_storage_bucket_iam_member" "ksa_model_fetcher_models_user" {
  bucket = google_storage_bucket.dyff["models"].name
  role   = "roles/storage.objectUser"
  # TODO: Don't hard-code KSA name
  member = "serviceAccount:${var.project}.svc.id.goog[workflows/model-fetcher]"
}
