# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

module "root" {
  source = "../.."

  environment = "production"

  google_cloud_service_account_file = var.google_cloud_service_account_file

  project = "production-dyff-862492"

  frontend_origin = var.frontend_origin

  google_cloud_storage_allowed_cors_origins = [var.frontend_origin]
}
