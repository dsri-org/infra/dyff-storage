# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "buckets" {
  value = module.root.buckets
}

output "hostname" {
  description = "hostname of the google s3 bucket"
  value       = module.root.hostname
}

output "access_id" {
  value     = module.root.access_id
  sensitive = true
}

output "secret" {
  value     = module.root.secret
  sensitive = true
}

output "backup_bucket" {
  value = module.root.backup_bucket
}

output "backup_access_id" {
  value     = module.root.backup_access_id
  sensitive = true
}

output "backup_secret" {
  value     = module.root.backup_secret
  sensitive = true
}
