# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  cluster_name       = "${var.environment}-dyff-cloud"
  deployment         = "dyff-cloud-storage"
  name               = "${var.environment}-${local.deployment}"
  region             = "us-central1"
  backup_region      = "us-west1"
  s3_storage_backend = "https://storage.googleapis.com"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
    manager     = "terraform"
  }

  bucket_ids = toset([
    "backup",
    "datasets",
    "measurements",
    "models",
    "modules",
    "outputs",
    "reports",
    "safetycases",
  ])
}
