# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "random_string" "backup" {
  length = 8

  special = false
  upper   = false
}

resource "google_kms_key_ring" "backup" {
  name     = "${local.name}-${local.backup_region}"
  location = local.backup_region
}

resource "google_kms_crypto_key" "backup" {
  name            = "${local.name}-${local.backup_region}"
  key_ring        = google_kms_key_ring.backup.id
  rotation_period = "7776000s"
}

resource "google_kms_crypto_key_iam_binding" "backup" {
  crypto_key_id = google_kms_crypto_key.backup.id
  role          = "roles/cloudkms.cryptoKeyEncrypterDecrypter"

  members = [
    "serviceAccount:${data.google_storage_project_service_account.this.email_address}",
  ]
}

# The 'backup' bucket contains a daily snapshot of all of the primary buckets.
# It's soft-delete period must be *longer* than that of the primary buckets.
resource "google_storage_bucket" "backup" {
  name = "backup-${random_string.backup.result}"

  force_destroy = false

  location = local.backup_region

  storage_class = "NEARLINE"

  uniform_bucket_level_access = true

  public_access_prevention = "enforced"

  labels = local.default_tags

  encryption {
    default_kms_key_name = google_kms_crypto_key.backup.id
  }

  soft_delete_policy {
    retention_duration_seconds = 604800 # 7 days
  }

  versioning {
    enabled = true
  }

  lifecycle_rule {
    condition {
      days_since_noncurrent_time = 7
    }
    action {
      type = "Delete"
    }
  }

  depends_on = [google_kms_crypto_key_iam_binding.backup]
}

resource "google_service_account" "backup" {
  account_id   = google_storage_bucket.backup.name
  display_name = google_storage_bucket.backup.name
}

resource "google_project_iam_custom_role" "gcloud_storage_rsync_source" {
  project     = var.project
  role_id     = "storage.rsyncSource"
  title       = "Minimal role for the 'source' side of rsync"
  description = "Grants the permissions that 'gcloud storage rsync' needs on the source buckets."
  permissions = [
    # roles/storage.objectViewer
    # These are only valid for org-level roles:
    # https://cloud.google.com/knowledge/kb/permission-error-for-custom-role-000004670
    # "resourcemanager.projects.get",
    # "resourcemanager.projects.list",
    # "storage.folders.get",
    # "storage.folders.list",
    "storage.managedFolders.get",
    "storage.managedFolders.list",
    "storage.objects.get",
    "storage.objects.list",
    # rsync requires buckets.get
    "storage.buckets.get",
    "storage.buckets.list",
  ]
}

resource "google_project_iam_custom_role" "gcloud_storage_rsync_destination" {
  project     = var.project
  role_id     = "storage.rsyncDestination"
  title       = "Minimal role for the 'destination' side of rsync"
  description = "Grants the permissions that 'gcloud storage rsync' needs on the destination buckets."
  permissions = [
    # roles/storage.objectUser
    # These are only valid for org-level roles:
    # https://cloud.google.com/knowledge/kb/permission-error-for-custom-role-000004670
    # "orgpolicy.policy.get",
    # "resourcemanager.projects.get",
    # "resourcemanager.projects.list",
    # "storage.folders.*",
    # "storage.multipartUploads.*", # presumably because of the .list ops
    "storage.managedFolders.create",
    "storage.managedFolders.delete",
    "storage.managedFolders.get",
    "storage.managedFolders.list",
    "storage.multipartUploads.abort",
    "storage.multipartUploads.create",
    "storage.objects.create",
    "storage.objects.delete",
    "storage.objects.get",
    "storage.objects.list",
    "storage.objects.restore",
    "storage.objects.update",
    # rsync requires buckets.get
    "storage.buckets.get",
    "storage.buckets.list",
  ]
}

# Grant necessary role on backup sources
resource "google_storage_bucket_iam_member" "backup_source" {
  for_each = google_storage_bucket.dyff

  bucket = each.value.name
  role   = google_project_iam_custom_role.gcloud_storage_rsync_source.name
  member = "serviceAccount:${google_service_account.backup.email}"
}

# Grant necessary role on backup destination
resource "google_storage_bucket_iam_member" "backup_destination" {
  bucket = google_storage_bucket.backup.name
  role   = google_project_iam_custom_role.gcloud_storage_rsync_destination.name
  member = "serviceAccount:${google_service_account.backup.email}"
}

# Allow the k8s service account to impersonate the IAM service account
resource "google_service_account_iam_member" "backup" {
  service_account_id = google_service_account.backup.name
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${var.project}.svc.id.goog[${kubernetes_service_account.backup.metadata[0].namespace}/${kubernetes_service_account.backup.metadata[0].name}]"
}

resource "google_storage_hmac_key" "backup" {
  service_account_email = google_service_account.backup.email
}

resource "kubernetes_namespace" "dyff_storage" {
  metadata {
    name = "dyff-storage"
    labels = {
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

resource "kubernetes_service_account" "backup" {
  metadata {
    name      = "backup"
    namespace = kubernetes_namespace.dyff_storage.metadata[0].name

    annotations = {
      "iam.gke.io/gcp-service-account" = "${google_service_account.backup.email}"
    }
  }
}

resource "kubernetes_cron_job_v1" "gcs_backup_replication" {
  for_each = google_storage_bucket.dyff

  metadata {
    name      = "gcs-backup-replication-${each.key}"
    namespace = kubernetes_namespace.dyff_storage.metadata[0].name

    labels = {
      "app.kubernetes.io/name" = "gcs-backup-replication"
    }
  }

  spec {
    schedule = "@daily"

    job_template {
      metadata {}

      spec {

        template {
          metadata {}

          spec {
            service_account_name = kubernetes_service_account.backup.metadata[0].name

            container {
              name  = "google-cloud-sdk"
              image = "google/cloud-sdk:503.0.0-stable"

              command = [
                "gcloud",
                "storage",
                "rsync",
              ]

              args = [
                "--recursive",
                "--delete-unmatched-destination-objects",
                "--quiet",
                "--verbosity=info",
                "gs://${each.value.name}/",
                "gs://${google_storage_bucket.backup.name}/${each.value.name}/",
              ]

              # gcloud CLI expects ~/.config/gcloud to be writeable
              env {
                name  = "CLOUDSDK_CONFIG"
                value = "/tmp/.config/gcloud"
              }

              resources {
                # limits = {
                #   "cpu"               = "500m"
                #   "ephemeral-storage" = "50Mi"
                #   "memory"            = "512Mi"
                # }
                requests = {
                  "cpu"               = "2000m"
                  "ephemeral-storage" = "512Mi"
                  "memory"            = "2Gi"
                }
              }

              security_context {
                allow_privilege_escalation = false

                capabilities {
                  drop = ["ALL"]
                }

                privileged                = false
                read_only_root_filesystem = true
                run_as_group              = 10001
                run_as_non_root           = true
                run_as_user               = 10001

                seccomp_profile {
                  type = "RuntimeDefault"
                }
              }

              volume_mount {
                name       = "tmp"
                read_only  = false
                mount_path = "/tmp"
              }
            }

            restart_policy = "OnFailure"

            volume {
              name = "tmp"
              empty_dir {}
            }
          }
        }
      }
    }
  }
}
