# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "buckets" {
  value = {
    for key, bucket in google_storage_bucket.dyff : key => merge(bucket, {
      s3_url = "s3://${bucket.name}"
    })
  }
}

output "hostname" {
  description = "hostname of the google s3 bucket"
  value       = local.s3_storage_backend
}

output "access_id" {
  value     = google_storage_hmac_key.key.access_id
  sensitive = true
}

output "secret" {
  value     = google_storage_hmac_key.key.secret
  sensitive = true
}

output "backup_bucket" {
  value = google_storage_bucket.backup
}

output "backup_access_id" {
  value     = google_storage_hmac_key.key.access_id
  sensitive = true
}

output "backup_secret" {
  value     = google_storage_hmac_key.key.secret
  sensitive = true
}
